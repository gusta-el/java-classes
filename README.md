# Java Classes

## Requerimentos
* [Eclipse IDE for Java](http://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2018-09/R/eclipse-java-2018-09-win32-x86_64.zip&mirror_id=576)
* [Java SE Development Kit 8u181](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [MySQL 8.0.12](https://dev.mysql.com/downloads/file/?id=479669)